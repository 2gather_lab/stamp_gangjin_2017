angular.module('stamp.controllers')
.controller('NoticeController', function($ionicPlatform, $ionicHistory, $rootScope, $scope, $http, $StorageUtil, $CommonUtil) {

  //LogUtil.debug("[Notice Controller]");

    $scope.noticePopup = {};
    $scope.noticeModalOpen = false;

    $ionicModal.fromTemplateUrl('templates/noticeModal.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.noticeModal = modal;
    });

    $scope.openNoticeModal = function(){
        $scope.noticeModalOpen = true;
        $scope.noticeModal.show();
    }

    $scope.closeNoticeModal = function() {
        if($scope.noticePopup.nomore == true){
            $StorageUtil.set($scope.noticePopup.notice_id, true);
        }
        $scope.noticeModalOpen = false;
        $scope.noticeModal.hide();
    };


    $ionicPlatform.onHardwareBackButton(function() {
    if($ionicHistory.currentStateName() == "app.rally"){
      ionic.Platform.exitApp()
    }else{
      $ionicHistory.goBack();
    }
    e.stopPropagation();
  });

  $scope.$on('$ionicView.loaded', function (viewInfo, state) {
  });

  try{
    $http({
      method: 'GET',
      url: 'https://stamp.2gather.us/openapi/notice/500'
    }).then(function(response) {
      if(response != undefined){
        $scope.noticeList = response.data.list;

        for(var notice in $scope.noticeList) {
          if(notice.popup == '1') {
              $scope.noticePopup = notice;
              if($StorageUtil.set($scope.noticePopup.notice_id) != true){
                  openNoticeModal();
                  break;
              }
          }
        }
        //LogUtil.debug("[Notice List]"+ JSON.stringify(response.data.list));
      }
    }, function(err) {
      //LogUtil.debug('[Notice List Error]', err);
    })

  }catch(err){
    //LogUtil.debug("[Notice List Exception]" + JSON.stringify(err));
  }
});
