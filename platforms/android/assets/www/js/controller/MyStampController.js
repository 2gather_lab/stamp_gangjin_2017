angular.module('stamp.controllers')
.controller('MyStampController', function($ionicPopup, $ionicPlatform, $ionicHistory, $rootScope, $scope, $http, $ionicModal) {

  LogUtil.debug("[MyStamp Controller]");

    $ionicPlatform.onHardwareBackButton(function() {
        if($ionicHistory.currentStateName() == "app.rally"){
            ionic.Platform.exitApp()
        }else{
            $ionicHistory.goBack();
        }
        e.stopPropagation();
    });

  $scope.reviewModalOpen = false;
  $scope.starValue = 5;
  $scope.message = "";
  $scope.rallyId = 500;
  $scope.spotId = 0;
  $scope.reviewId = 0;

  $ionicModal.fromTemplateUrl('templates/reviewModal.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.reviewModal = modal;
  });

  $scope.$on('$ionicView.leave', function (viewInfo, state) {
    $scope.closeReviewModal();
    $scope.reviewModal.remove();
  });


  LogUtil.debug("[MyStamp]AccessToken:"+$scope.tgToken.access_token);

  $scope.initialize = function () {

    try{
      $http({
        method: 'POST',
        url: 'https://stamp.2gather.us/api/v1/mystamps',
        headers: {
          'Content-Type': 'application/json; charset=utf-8',
          'Authorization': 'Bearer ' + $scope.tgToken.access_token
        },
        data: {
          "rallyId": 500
        }
      }).then(function(response) {
        if(response != undefined){
          $scope.stampList = response.data.list;
          LogUtil.debug("[MyStamp]"+ JSON.stringify(response.data.list));
        }
      }, function(err) {
        LogUtil.debug('[MyStamp Error]', err);
      })

    }catch(err){
      LogUtil.error("[MyStamp Exception]" + JSON.stringify(err));
    }

  }

  $scope.initialize();

  $scope.writeReview = function() {
    try {
      $http({
        method: 'POST',
        url: 'https://stamp.2gather.us/api/v1/spotReviews',
        headers: {
          'Content-Type': 'application/json; charset=utf-8',
          'Authorization': 'Bearer ' + $scope.tgToken.access_token
        },
        data: {
          "spotReviewId": $scope.reviewId,
          "rallyId": $scope.rallyId,
          "spotId": $scope.spotId,
          //"userId": 0,
          "rating": $scope.starValue,
          "content": document.getElementById("reviewText").value,
          "reviewType": "BASIC"
        }
      }).then(function(response) {
        LogUtil.debug("writeReview response " +  JSON.stringify(response));
        if(response != undefined){
          if(response.data.status == 200){
            LogUtil.debug("writeReview OK " +  JSON.stringify(response.data));

            //$scope.joinEvent("REV");

            $scope.initialize();
            $scope.closeReviewModal();
            //$state.transitionTo($state.current, null, { reload: true, inherit: false, notify: false });
          }
        }
      }, function(err) {
        LogUtil.debug('writeReview ERR' + err);
      })
    } catch(e) {
      LogUtil.debug('writeReview Exception' + e);
    }

  };

  $scope.openReviewModal = function(rallyId, spotId, reviewId, reviewRating, reviewText){
    LogUtil.debug("Review : "+ rallyId + ", "+ spotId + ", "+ reviewId + ", "+ reviewRating + ", "+ reviewText);
    $scope.reviewModalOpen = true;
    $scope.reviewModal.show();
    $scope.setStar(reviewRating);
    $scope.rallyId = rallyId;
    $scope.spotId = spotId;
    $scope.reviewId = reviewId;
    document.getElementById("reviewText").value = reviewText;

    //document.getElementById("reviewText").focus();
  }

  $scope.closeReviewModal = function() {
    $scope.reviewModalOpen = false;
    $scope.reviewModal.hide();
  };

  $scope.setStar = function(value){
    LogUtil.debug("STAR");
    $scope.starValue = value;
    var img = document.getElementsByClassName("starImg");
    //img/star_zero.png
    for(var i = 0 ;i< img.length; i++){
      if(i<value){
        img[i].src = "img/star_full.png"
      }else{
        img[i].src = "img/star_zero.png"
      }
    }
  }
});
