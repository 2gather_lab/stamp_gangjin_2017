angular.module('stamp', ['ionic', 'stamp.controllers'])
  .run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
      if(window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
      }
      if(window.StatusBar) {
        StatusBar.styleDefault();
      }
    });
  })
  .config(function($ionicConfigProvider, $stateProvider, $urlRouterProvider, $cordovaFacebookProvider) {
      $ionicConfigProvider.backButton.previousTitleText(false).text('뒤로').icon('ion-ios-arrow-back');
      $ionicConfigProvider.navBar.alignTitle('center');
    var appID = 1057272771002922;
    var version = "v2.0"; // or leave blank and default is v2.0
    $cordovaFacebookProvider.browserInit(appID, version);

    $stateProvider
      .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'MainController'
      })
      .state('app.rally', {
        url: '/rally',
        views: {
          'menuContent': {
            templateUrl: 'templates/rally.html',
            controller: 'RallyController'
          }
        }
      })
        .state('app.stamp', {
            url: '/stamp/:rallyId',
            views: {
                'menuContent': {
                    templateUrl: 'templates/stamp.html',
                    controller: 'StampController'
                }
            }
        })
      .state('app.rallyDetail', {
        url: '/rallyDetail/:rallyId',
        views: {
          'menuContent': {
            templateUrl: 'templates/rallyDetail.html',
            controller: 'RallyDetailController'
          }
        }
      })
       .state('app.myStamp', {
         url: '/myStamp',
         views: {
           'menuContent': {
             templateUrl: 'templates/myStamp.html',
             controller: 'MyStampController'
           }
         }
       })
       .state('app.event', {
         url: '/event',
         views: {
           'menuContent': {
             templateUrl: 'templates/event.html',
             controller: 'EventController'
           }
         }
       })
       .state('app.notice', {
         url: '/notice',
         views: {
           'menuContent': {
             templateUrl: 'templates/notice.html',
             controller: 'NoticeController'
           }
         }
       })
      // .state('app.map', {
      //   url: '/map',
      //   views: {
      //     'menuContent': {
      //       templateUrl: 'templates/map.html',
      //       controller: 'MapController'
      //     }
      //   }
      // })
    ;
    $urlRouterProvider.otherwise('/app/rally');
  });

document.addEventListener("deviceready", onDeviceReady, false);
function onDeviceReady() {
    window.open = cordova.InAppBrowser.open;
}