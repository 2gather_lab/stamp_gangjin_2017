angular.module('stamp.controllers')
    .controller('RallyDetailController', function($ionicPlatform, $ionicHistory, $rootScope, $scope, $http, $StorageUtil, $CommonUtil, $ionicModal, $cordovaGeolocation) {

      LogUtil.debug('[RallyDetail Controller]');

      $ionicPlatform.onHardwareBackButton(function() {
        if($ionicHistory.currentStateName() == "app.rally"){
          ionic.Platform.exitApp()
        }else{
          $ionicHistory.goBack();
        }
        e.stopPropagation();
      });

      //$scope.openMapWindow = function () {
      //  window.open('http://maps.google.com/?q='+$scope.showSpot.latitude+','+$scope.showSpot.longitude, '_blank', 'location=yes');
      //}

      $scope.titleName = $rootScope.titleName;
      $scope.tgToken = $rootScope.tgToken;
        $scope.existInformation = false;

        if($scope.showSpot == null || $scope.showSpot == undefined) {
            $scope.showSpot = ($rootScope.showSpot != undefined) ? $rootScope.showSpot : $StorageUtil.getObject('showSpot');
        }

        var latLong = new google.maps.LatLng($scope.showSpot.latitude, $scope.showSpot.longitude);

        var mapOptions = {
            center: latLong,
            zoom: 13,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById('map'), mapOptions);

        var marker = new google.maps.Marker({
            position: latLong,
            map: map,
            title: $scope.showSpot.spot_name
        });

        try{
            $http({
                method: 'GET',
                url: 'http://stamp.2gather.us/openapi/spotReviews/500/'+ $scope.showSpot.spotId,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                },
            }).then(function(response) {
                if(response != undefined){
                    $scope.reviewList = response.data.list;
                }
            }, function(err) {
                LogUtil.debug('[RallyReview Error]', err);
            })

        }catch(err){
            LogUtil.error("[RallyReview Exception]" + JSON.stringify(err));
        }

      $scope.$on('$ionicView.loaded', function (viewInfo, state) {
          $scope.spotInformation = $scope.showSpot.spotInfomation;
          if($scope.getCheckSpotInformation($scope.showSpot)) $scope.existInformation = true;
      });

      $scope.rallyInfoImage = function() {
        return $scope.getRallyImage('E');
      }

      $scope.rallyMainImage = function() {
        return $scope.getRallyImage('I');
      }

      $scope.rallyMapImage = function() {
        return $scope.getRallyImage('M');
      }

      $scope.getRallyImage = function(type) {
        try {
          for (i = 0; i < $scope.showSpot.image.length; i++) {
            if ($scope.showSpot.image[i].type == type) {
              //console.log(rally.image[i].type);
              return $scope.showSpot.image[i].imageName + '.' + $scope.showSpot.image[i].extension;
            }
          }
        }catch(e){
          return '';
        }
      }

        $scope.getCheckSpotInformation = function(rallySpot){
            if(rallySpot.spotInfomation == undefined || rallySpot.spotInfomation == null){
                return false;
            }
            return true;
        }
    });
