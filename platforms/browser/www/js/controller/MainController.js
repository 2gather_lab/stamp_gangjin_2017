angular.module('stamp.controllers', ['ionic', 'stamp.utils','ngCordova'])
    .controller('MainController', function($rootScope, $scope, $ionicModal, $http, $timeout, $StorageUtil, $cordovaFacebook, $state, $ionicSideMenuDelegate, $cordovaToast, $cordovaDevice, $CommonUtil, $cordovaGeolocation, $ionicPlatform) {

      LogUtil.debug("[Main Controller]");

      $rootScope.checkLogin = false;
      $scope.loginData = {};
      $scope.statusText = "로그인";

      $ionicModal.fromTemplateUrl('templates/login.html', {
        scope: $scope
      }).then(function(modal) {
        $scope.modal = modal;
      });

      $scope.Cs = {
        checkLogin : "CHECKLOGIN",
        lastLoginType : "LASTLOGINTYPE",
        lastLoginData : "LASTLOGINDATA",
        fbToken : "FBTOKEN",
        fbInfo : "FBINFO",
        tgToken : "TGTOKEN",
      }

      $scope.oneLoginCheck = false;
      $scope.fbToken = {};
      $scope.fbInfo = {};
      $scope.tgToken = {};
      $scope.lastLoginData = {};
      $scope.error = "";

        function gpsOnFail(err) {
            LogUtil.error("[GPS Error]" + JSON.stringify(err));
        }
        function gpsOnSuccess(position) {
            $rootScope.lastLocation = { latitude: position.coords.latitude , longitude: position.coords.longitude };
            //LogUtil.error("[GPS Fount]" + position.coords.latitude + '/'+ position.coords.longitude);
        };

        var watchId = navigator.geolocation.watchPosition(gpsOnSuccess, gpsOnFail, {
            timeout : 5000,
            enableHighAccuracy: false,
            maximumAge: Infinity,
        });

      $scope.$on('$ionicView.leave', function (viewInfo, state) {
        $scope.closeModal();
      });

      $scope.$on('$ionicView.enter', function (viewInfo, state) {
      });

      $scope.$on('$ionicView.loaded', function (viewInfo, state) {
        if($rootScope.signInTry != true) {
          $rootScope.signInTry = true;

          try{
            LogUtil.debug("[SignIn Init]");
            $scope.fbToken = $StorageUtil.getObject($scope.Cs.fbToken);
            $scope.fbInfo = $StorageUtil.getObject($scope.Cs.fbInfo);
            $scope.tgToken = $StorageUtil.getObject($scope.Cs.tgToken);
            $scope.lastLoginData = $StorageUtil.getObject($scope.Cs.lastLoginData);
            LogUtil.debug("[SignIn Data]"+ JSON.stringify($scope.tgToken));
          }catch(err){
            LogUtil.debug("[SignIn Init Exception]"+err);
          }

          $scope.oneLoginCheck = true;

          var lastType = $StorageUtil.get($scope.Cs.lastLoginType, "");

          if(lastType == "MA"){

            LogUtil.debug("[SignIn MA]");

            if(Object.keys($scope.lastLoginData).length > 0 && Object.keys($scope.tgToken).length > 0){

              $rootScope.lastLoginType = "MA";
              $rootScope.tgToken = $scope.tgToken;

              //$scope.getRefreshTgToken();

              $http({
                method: 'POST',
                url: 'https://stamp.2gather.us/oauth/token?refresh_token='+$scope.tgToken.refresh_token+'&grant_type=refresh_token&username='+$scope.lastLoginData.email+'&password='+$scope.lastLoginData.password+'&scope=read write',
                headers: {
                  'Content-Type': 'Content-Type: application/x-www-form-urlencoded',
                  'Authorization': 'Basic R0FOR0pJTjowZWYzOTVjNjdjYTQyNzg1MWFhZTg0MzhjMmUyNWY4ZDU4NjM4MzI2ZmJkNWU4YTQzN2JhMjJjNjM1NDE3MWM4'
                }
              }).then(function(response) {
                if(response != undefined){

                  LogUtil.debug("[SignIn Refresh Token] - " +JSON.stringify(response.data));
                  $scope.tgToken = response.data;
                  $StorageUtil.setObject($scope.Cs.tgToken, $scope.tgToken);
                  $rootScope.tgToken = response.data;

                    $rootScope.checkLogin = true;
                    $StorageUtil.set($scope.Cs.checkLogin, true);
                    $scope.statusText = "로그아웃";

                  $scope.$broadcast("onLoginUser", "");
                }
              }, function(err) {
                LogUtil.debug("[SignIn Refresh Token Error] - " +JSON.stringify(err));
              })

            }else{
              LogUtil.debug("[SignIn]No Sign information");
              $scope.goSignIn();
            }
          } else{
            LogUtil.debug("[SignIn]No SignIn Type");
            $scope.goSignIn();
          }
        }
      });

      $scope.emailCheck = function(strValue)
      {
        var regExp = /[0-9a-zA-Z][_0-9a-zA-Z-]*@[_0-9a-zA-Z-]+(\.[_0-9a-zA-Z-]+){1,2}$/;
        if(strValue.length == 0)
        {
          $cordovaToast.showLongCenter('메일 주소를 확인 바랍니다.');
          return false;
        }
        if (!strValue.match(regExp))
        {
          $cordovaToast.showLongCenter('메일 주소를 확인 바랍니다.');
          return false;
        }
        return true;
      }

      $scope.goSignIn = function(){
        LogUtil.error('GoSignIn');
        $scope.closeModal();
        $ionicModal.fromTemplateUrl('templates/login.html', {
          scope: $scope
        }).then(function(modal) {
          $scope.modal = modal;
          $scope.modal.show();
        });
      }

      $scope.goSignUp = function(){
        $scope.closeModal();
        $ionicModal.fromTemplateUrl('templates/join.html', {
          scope: $scope
        }).then(function(modal) {
          $scope.modal = modal;
          $scope.modal.show();
        });
      }

        $scope.goFindPassword = function(){
            $scope.closeModal();
            $ionicModal.fromTemplateUrl('templates/password.html', {
                scope: $scope
            }).then(function(modal) {
                $scope.modal = modal;
                $scope.modal.show();
            });
        }

      $scope.goTerm = function(){
        $scope.closeModal();
        $ionicModal.fromTemplateUrl('templates/term.html', {
          scope: $scope
        }).then(function(modal) {
          $scope.modal = modal;
          $scope.modal.show();
        });
      }

      $scope.doSignIn = function(loginEmail, loginPass){

          if(loginEmail != undefined) $scope.loginData.email = loginEmail;
          if(loginPass != undefined) $scope.loginData.password = loginPass;

          LogUtil.debug('[Sign In Try]'+$scope.loginData.email+'/'+$scope.loginData.password);

        if(!$scope.emailCheck($scope.loginData.email)){
          $scope.error = "메일주소확인바람";
          return;
        }

        $http({
          method: 'POST',
          url: 'https://stamp.2gather.us/oauth/token?grant_type=password&username='+$scope.loginData.email+'&password='+$scope.loginData.password+'&scope=read write',
          headers: {
            'Content-Type': 'Content-Type: application/x-www-form-urlencoded',
              'Authorization': 'Basic R0FOR0pJTjowZWYzOTVjNjdjYTQyNzg1MWFhZTg0MzhjMmUyNWY4ZDU4NjM4MzI2ZmJkNWU4YTQzN2JhMjJjNjM1NDE3MWM4'
          }
        }).then(function(response) {
          if(response != undefined){

            if(response.status == 401 || response.status == 400){
              LogUtil.debug("로그인 실패 " + JSON.stringify(response));
              $cordovaToast.showLongCenter("로그인이 실패했습니다. 다시 시도 바랍니다.");
              return
            }

            $rootScope.lastLoginType = "MA";
            $StorageUtil.set($scope.Cs.lastLoginType, "MA");

            $rootScope.checkLogin = true;
            $StorageUtil.set($scope.Cs.checkLogin, true);

            $StorageUtil.setObject($scope.Cs.lastLoginData, $scope.loginData);

            $scope.tgToken = response.data;
            $StorageUtil.setObject($scope.Cs.tgToken, $scope.tgToken);

            $rootScope.tgToken = response.data;


            $scope.statusText = "로그아웃";

            $scope.$broadcast("onLoginUser", "");

            $state.go("app.rally");

            $scope.closeModal();
            LogUtil.debug("[Close Modal]");
          }
        }, function(err) {
          LogUtil.error('[tgSignIn HTTP ERROR] - ' + JSON.stringify(err));
          $cordovaToast.showLongCenter("로그인이 실패했습니다. 다시 시도 해주세요.");
        })
      }

      $scope.doSignUp = function(){
        if($scope.loginData.agree != true){
          $cordovaToast.showLongCenter("약관에 동의를 하셔야 가입하실 수 있습니다.");
          return
        }

        if($scope.loginData.name == null){
          $cordovaToast.showLongCenter("이름을 확인해 주세요.");
          return
        }

        if($scope.loginData.email == null || !$scope.emailCheck($scope.loginData.email)){
          $cordovaToast.showLongCenter("메일 주소를 확인해 주세요.");
          LogUtil.debug("[doSignUp]메일주소확인바람");
          return
        }

        if($scope.loginData.password == null){
          $cordovaToast.showLongCenter("비밀번호를 확인해 주세요.");
          return
        }

        $http({
          method: 'POST',
          url: 'https://stamp.2gather.us/api/v1/signup',
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
          data: {
            "userType": "MA",
            "userCode": $scope.loginData.email,
            "clientId": "GANGJIN",
            "name": $scope.loginData.name,
            "email": $scope.loginData.email,
            "password": $scope.loginData.password,
          }
        }).then(function(response) {
          if(response != undefined){
            if(response.data.status == 200){
              $cordovaToast.showLongCenter("가입을 축하드립니다.");

              $scope.doSignIn($scope.loginData.email, $scope.loginData.password);

            }else{
              $cordovaToast.showLongCenter('회원 가입에 실패했습니다.\n'+ ((response.data.error.message != undefined) ? response.data.error.message : '관리자에게 문의 해주세요.'));
            }
          }
        }, function(err) {
          LogUtil.debug("[SignUp Error Exception]", err);
        })
      }

      $scope.openModal = function() {
        if($rootScope.checkLogin){
          $scope.logoutDataReset();
        }else{
            $scope.goSignIn();
        }
      }

      $scope.closeModal = function() {
        try {
          $scope.modal.hide();
        } catch(e) {
        }
      };

      $scope.logoutDataReset = function(){
        $rootScope.checkLogin = false;
        $StorageUtil.set($scope.Cs.checkLogin, false);

        $rootScope.lastLoginType = "";
        $StorageUtil.set($scope.Cs.lastLoginType, "");
        $scope.statusText = "로그인";
        $scope.fbToken = {};
        $scope.fbInfo = {};
        $scope.tgToken = {};
        $scope.lastLoginData = {};
        $StorageUtil.setObject($scope.Cs.fbToken, {});
        $StorageUtil.setObject($scope.Cs.fbInfo, {});
        $StorageUtil.setObject($scope.Cs.tgToken, {});
        $StorageUtil.setObject($scope.Cs.lastLoginData, {});

        $scope.$broadcast("onLoginUser", "");

        $scope.goSignIn();
      }

      $scope.getRefreshTgToken = function(tgToken){
        $http({
          method: 'POST',
          url: 'https://stamp.2gather.us/oauth/token?refresh_token='+tgToken.refresh_token+'&grant_type=refresh_token&username='+$scope.fbInfo.id+'&password=facebook&scope=read write',
          headers: {
            'Content-Type': 'Content-Type: application/x-www-form-urlencoded',
            'Authorization': 'Basic R0FOR0pJTjowZWYzOTVjNjdjYTQyNzg1MWFhZTg0MzhjMmUyNWY4ZDU4NjM4MzI2ZmJkNWU4YTQzN2JhMjJjNjM1NDE3MWM4'
          }
        }).then(function(response) {
          if(response != undefined){

            LogUtil.debug("[REFRESH TG TOKEN] - " +JSON.stringify(response.data));

            $scope.tgToken = response.data;
            $StorageUtil.setObject($scope.Cs.tgToken, $scope.tgToken);
            $rootScope.tgToken = response.data;
          }
        }, function(err) {
          LogUtil.debug("[ERR REFRESH TG TOKEN] - " +JSON.stringify(err));
        })
      }

      $scope.fbSignCheck = function () {
        try {

          if($rootScope.checkLogin){
            LogUtil.debug("GOTO LOGOUT")
            $scope.logoutDataReset()
            $cordovaFacebook.logout()
              .then(function(success) {
                LogUtil.debug("GOTO LOGOUT success")
                // success
              }, function (error) {
                LogUtil.error("GOTO LOGOUT error")
                // error
              });
          }else{
            $cordovaFacebook.login(["public_profile", "email"])
              .then(function(success) {
                LogUtil.debug("FB SignCheck Success"+JSON.stringify(success));
                $scope.fbToken = success.authResponse;
                $StorageUtil.setObject($scope.Cs.fbToken, $scope.fbToken);
                $scope.getFbInfo();
              }, function (error) {
                LogUtil.debug("FB Login Error"+JSON.stringify(error));
              });
          }
          LogUtil.debug("FB Login Success");
        } catch(e) {
          LogUtil.debug("FB Login Err - "+ e);
        }

      };

      $scope.getFbInfo = function () {
        LogUtil.debug('FB Get Info Start');
        $cordovaFacebook.api("me/?fields=id,name,email", ["public_profile", "email"])
          .then(function (success) {
            LogUtil.debug("FB Get Info Success"+JSON.stringify(success));
            $scope.fbInfo = success;
            $StorageUtil.setObject($scope.Cs.fbInfo, $scope.fbInfo);
            $scope.fbSignUp();
          }, function (error) {
            LogUtil.debug("FB Get Info Error"+JSON.stringify(error));
          });
      };

      $scope.fbSignUp = function () {
        LogUtil.debug('FBsignUp Start');
        $http({
          method: 'POST',
          url: 'https://stamp.2gather.us/api/v1/signup',
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
          data: {
            "userType": "FB",
            "userCode": $scope.fbInfo.id,
            "clientId": "GANGJIN",
            "email": $scope.fbInfo.email,
            "name": $scope.fbInfo.name,
          }
        }).then(function(response) {
          if(response != undefined){
            LogUtil.debug("signUp OK - " + JSON.stringify(response));
            if(response.data.status == 200){
                $scope.closeModal();
              LogUtil.debug("2gather GET TOKEN");
              //2gather GET TOKEN
              $scope.fbSignIn();
            }
          }
        }, function(err) {
          LogUtil.debug('signUp ERR ', err);
        })
      }

      $scope.fbSignIn = function () {
        $http({
          method: 'POST',
          url: 'https://stamp.2gather.us/oauth/token?grant_type=password&username='+$scope.fbInfo.id+'&password=facebook&scope=read write',
          headers: {
            'Content-Type': 'Content-Type: application/x-www-form-urlencoded',
            'Authorization': 'Basic R0FOR0pJTjowZWYzOTVjNjdjYTQyNzg1MWFhZTg0MzhjMmUyNWY4ZDU4NjM4MzI2ZmJkNWU4YTQzN2JhMjJjNjM1NDE3MWM4'
          }
        }).then(function(response) {
          if(response != undefined){
            LogUtil.debug(JSON.stringify(response));

            $rootScope.lastLoginType = "FB";
            $StorageUtil.set($scope.Cs.lastLoginType, "FB");

            $rootScope.checkLogin = true;
            $StorageUtil.set($scope.Cs.checkLogin, true);

            $scope.tgToken = response.data;
            $StorageUtil.setObject($scope.Cs.tgToken, $scope.tgToken);

            $rootScope.tgToken = response.data;

            $scope.statusText = "로그아웃";

            $scope.$broadcast("onLoginUser", "");

            $state.go("app.rally");

            $scope.closeModal();
            LogUtil.debug("[Close Modal]");
          }
        }, function(err) {
          LogUtil.error('[tgSignIn HTTP ERROR] - ' + JSON.stringify(err));
        })
      }

      $scope.goMyStamp = function(){
          if($rootScope.checkLogin) {
              $state.transitionTo('app.myStamp', '');
          }else{
              $scope.goSignIn();
          }
      }

      $scope.goEvent = function(){
          if($rootScope.checkLogin) {
              $state.transitionTo('app.event', {});
          }else{
              $scope.goSignIn();
          }
      }

      $scope.goNotice = function() {
        $state.transitionTo('app.notice');
      }


        $scope.doFindPassword = function(loginEmail){

            if(loginEmail != undefined) $scope.loginData.email = loginEmail;

            if(!$scope.emailCheck($scope.loginData.email)){
                $scope.error = "메일주소확인바람";
                return;
            }

            $http({
                method: 'POST',
                url: 'https://stamp.2gather.us/openapi/mail/password',
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                },
                data: {
                    "from": "master@2gather.us",
                    "email": $scope.loginData.email,
                }
            }).then(function(response) {
                if(response != undefined) {
                    if (response.data == '1') {
                        $cordovaToast.showLongCenter("요청하신 메일로 비밀번호가 전송됐습니다.");
                        return;
                    } else {
                        $cordovaToast.showLongCenter("비밀번호 찾기가 실패했습니다.\n다시 시도 바랍니다.");
                        return;
                    }
                }
            }, function(err) {
                $cordovaToast.showLongCenter("비밀번호 찾기가 실패했습니다.\n다시 시도 해주세요.");
            })
        }
    });
