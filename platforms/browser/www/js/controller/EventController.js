angular.module('stamp.controllers')
.controller('EventController', function($ionicPlatform, $ionicHistory, $rootScope, $scope, $http, $StorageUtil, $CommonUtil, $ionicModal, $cordovaToast, $state) {

    LogUtil.debug("[Event Controller]");

    $ionicPlatform.onHardwareBackButton(function() {
        if($ionicHistory.currentStateName() == "app.rally"){
            ionic.Platform.exitApp()
        }else{
            $ionicHistory.goBack();
        }
        e.stopPropagation();
    });

    $scope.eventModalOpen = false;
    $scope.apply = {};
    $scope.eventId = 0;

    $ionicModal.fromTemplateUrl('templates/eventModal.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.eventModal = modal;
    });

    $scope.openEventModal = function(){
        $scope.eventModalOpen = true;
        $scope.eventModal.show();
    }

    $scope.closeEventModal = function() {
        $scope.eventModalOpen = false;
        $scope.eventModal.hide();
    };

  $scope.$on('$ionicView.loaded', function (viewInfo, state) {
  });

  try{
    $http({
      method: 'GET',
      url: 'https://stamp.2gather.us/openapi/event/500'
    }).then(function(response) {
      if(response != undefined){
        $scope.eventList = response.data.list;
        LogUtil.debug("[Event List]"+ JSON.stringify(response.data.list));
      }
    }, function(err) {
      LogUtil.debug('[Event List Error]', err);
    })

  }catch(err){
    LogUtil.debug("[Event List Exception]" + JSON.stringify(err));
  }


    $scope.goEvent = function(eventId){
        try {
            $scope.eventId = eventId;
            $http({
                method: 'POST',
                url: 'https://stamp.2gather.us/api/v1/rallyEvents/applyCheck',
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Authorization': 'Bearer ' + $scope.tgToken.access_token
                },
                data: {
                    'rallyId': 500,
                    'eventId': eventId
                }
            }).then(function(response) {
                if(response != undefined){
                    LogUtil.debug('r'+JSON.stringify(response));
                    if(response.status == 200){
                        if(response.data == '1') {
                            $scope.openEventModal();
                        } else if(response.data == '2') {
                            $cordovaToast.showLongCenter("이미 이벤트에 응모 하셨습니다.");
                        } else if(response.data == '0') {
                            $cordovaToast.showLongCenter("아직 이벤트에 참여하실 수 없습니다.");
                        } else {
                            $cordovaToast.showLongCenter('이벤트 응모에 실패했습니다.');
                        }
                    }else{
                        $cordovaToast.showLongCenter('이벤트 응모에 실패했습니다.');
                    }
                }
            }, function(err) {
              LogUtil.debug('e2'+JSON.stringify(err));
                $cordovaToast.showLongCenter('이벤트 응모에 실패했습니다.');
            });
        } catch(e) {
            LogUtil.debug('e1'+JSON.stringify(e));
            $cordovaToast.showLongCenter('이벤트 응모에 실패했습니다.');
        }
    }

    $scope.doApply = function(){
        if($scope.apply.agree != true){
            $cordovaToast.showLongCenter("약관에 동의를 하셔야 이벤트를 신청하실 수 있습니다.");
            return
        }

        if($scope.apply.name == ''){
            $cordovaToast.showLongCenter("이름을 입력하셔야 응모하실 수 있습니다.");
            return
        }

        if($scope.apply.phone == ''){
            $cordovaToast.showLongCenter("연락처를 입력하셔야 응모하실 수 있습니다.");
            return
        }

        if($scope.apply.address == ''){
            $cordovaToast.showLongCenter("주소를 입력하셔야 응모하실 수 있습니다.");
            return
        }
        try {
            $http({
                method: 'POST',
                url: 'https://stamp.2gather.us/api/v1/rallyEvents/apply',
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Authorization': 'Bearer ' + $scope.tgToken.access_token
                },
                data: {
                    'rallyId': 500,
                    'eventId': $scope.eventId,
                    'name': $scope.apply.name,
                    'phone': $scope.apply.phone,
                    'address': $scope.apply.address
                }
            }).then(function(response) {
                if(response != undefined){
                    if(response.status == 200){
                        $cordovaToast.showLongCenter("이벤트에 성공적으로 응모되셨습니다.");
                        $scope.closeEventModal();
                    }else{
                        $cordovaToast.showLongCenter('이벤트 응모에 실패했습니다.');
                    }
                }
            }, function(err) {
              LogUtil.debug(JSON.stringify(err));
                $cordovaToast.showLongCenter('이벤트 응모에 실패했습니다.');
            });
        } catch(e) {
            LogUtil.debug(JSON.stringify(e));
            $cordovaToast.showLongCenter('이벤트 응모에 실패했습니다.');
        }
    }

    $scope.toDate = function(str) {
        return str.substring(0, 4)+'.'+str.substring(4,6)+'.'+str.substring(6,8);
    }
});
