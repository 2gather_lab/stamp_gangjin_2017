angular.module('stamp.utils')

  .factory('$CommonUtil', ['$window', function($window) {
    return {
      isLocalTest: function() {
        // false = 배포 / true = 테스트
        return false;
        //return true;
      },
      isRequireGps: function() {
        // false = GPS 없이 위치 정보 없이 스탬핑 / true = GPS 정보 없을시 스탬핑 불가
        //return false;
        return true;
      }
    }
  }]);

