angular.module('stamp.utils')

  .factory('$MathUtill', ['$window', function($window) {
    return {
      twoPointDistance : function(p1, p2){
        return Math.sqrt(Math.pow((p2.x - p1.x), 2) + Math.pow((p2.y - p1.y), 2));
      },
      twoPointMedian : function(p1, p2){
        return {'x':(p1.x + p2.x)/2, 'y':(p1.y + p2.y)/2};
      },
      angleByMovingPoint : function(fixPtr, movPtr, angle){
        var fix_x = fixPtr.x;
        var fix_y = fixPtr.y;

        var mov_x = movPtr.x;
        var mov_y = movPtr.y;

        var rx = (mov_x-fix_x) * Math.cos(angle) - (mov_y-fix_y) * Math.sin(angle) + fix_x;
        var ry = (mov_x-fix_x) * Math.sin(angle) + (mov_y-fix_y) * Math.cos(angle) + fix_y;

        return {'x':rx, 'y':ry};
      },
      toDegrees : function(angle) {
        return angle * (180 / Math.PI);
      },
      angleForMove : function(fixPtr, movPtr, elsPtr, cenPtr){
        var distance = this.twoPointDistance(fixPtr, movPtr).toFixed(2);


        LogUtil.debug(distance);
        LogUtil.debug(fixPtr.x);

        var v_x = parseFloat(fixPtr.x) + parseFloat(distance);
        LogUtil.debug(v_x);
        var v_y = fixPtr.y;

        var reverse = false;

        var cal_x = movPtr.x - fixPtr.x;
        var cal_y = movPtr.y - fixPtr.y;

        if(cal_x > 0 && cal_y > 0){
          reverse = true;
        }else if(cal_x > 0 && cal_y < 0){
          reverse = false;
        }else if(cal_x < 0 && cal_y > 0){
          reverse = true;
        }else if(cal_x < 0 && cal_y < 0){
          reverse = false;
        }
        LogUtil.debug("cal_x_y : "+ cal_x + " / "+ cal_y + " :: " + reverse);

        var virPtr = {'x':v_x, 'y':v_y};
        LogUtil.debug("virPtr : "+ virPtr.x + " / "+ virPtr.y);

        var medPtr = this.twoPointMedian(movPtr, virPtr);
        LogUtil.debug("medPtr _ movPtr <-> virPtr : "+ medPtr.x + " / "+ medPtr.y);

        var height = this.twoPointDistance(medPtr, fixPtr);
        var width = this.twoPointDistance(movPtr, medPtr);
        LogUtil.debug("height : "+ height + " / width : "+ width);


        var tanDe = this.toDegrees(Math.atan(height / width));
        var namAn = 90 - tanDe;
        var piAn = namAn * 2;

        LogUtil.debug("tanDe : "+ tanDe + " / namAn : " + namAn + " / piAn : " + piAn);

        if(reverse){
          piAn = -piAn;
        }
        var angle = piAn * Math.PI/180;

        LogUtil.debug("angle : " + angle);

        var mMov = this.angleByMovingPoint(fixPtr, movPtr, angle);
        var mEls = this.angleByMovingPoint(fixPtr, elsPtr, angle);
        var mCen =  this.angleByMovingPoint(fixPtr, cenPtr, angle);

        var success = false;
        if(mEls.y > mMov.y && mCen.y > mMov.y){
          success = true;
        }

        if(mEls.x > mCen.x){
          return {'fixPtr':fixPtr, 'MovPtr':mMov, 'ElsPtr':mCen, 'CenPtr':mEls, 'success':success, 'PiAng':piAn };
        }else{
          return {'fixPtr':fixPtr, 'MovPtr':mMov, 'ElsPtr':mEls , 'CenPtr':mCen, 'success':success, 'PiAng':piAn };
        }
      }
    }
  }]);
