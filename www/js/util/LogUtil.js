var LogUtil = {
    _INFO_LOG : true,
    _WARN_LOG : true,
    _ERROR_LOG : true,
    _DEBUG_LOG : true,
    _log : function(flag, tag, msg, sendServer, target){
        if(flag){
            var logMsg = "__STAMP__ [" + tag + "] " + LogUtil.getTimeStamp() + " : " + msg;
            console.log(logMsg);
            if(target != undefined){
                target.append("\r\n" + logMsg);
            }
            if(sendServer || false){
                //서버로 로그 전송
            }
        }
    },
    info : function(msg, sendServer, target){
        this._log(this._INFO_LOG, "INFO", msg, sendServer, target);
    },
    warning : function(msg, sendServer, target){
        this._log(this._WARN_LOG, "WARN", msg, sendServer, target);
    },
    error : function(msg, sendServer, target){
        this._log(this._ERROR_LOG, "ERRO", msg, sendServer, target);
    },
    debug : function(msg, sendServer, target){
        this._log(this._DEBUG_LOG, "DEBU", msg, sendServer, target);
    },
    getTimeStamp : function(){
      var d = new Date();
      var s =
        this.paddingText(d.getFullYear(), 4) + '-' +
        this.paddingText(d.getMonth() + 1, 2) + '-' +
        this.paddingText(d.getDate(), 2) + ' ' +
        this.paddingText(d.getHours(), 2) + ':' +
        this.paddingText(d.getMinutes(), 2) + ':' +
        this.paddingText(d.getSeconds(), 2);
      return s;
    },
    paddingText : function(n, digits){
      var zero = '';
      n = n.toString();

      if (n.length < digits) {
        for (i = 0; i < digits - n.length; i++)
          zero += '0';
      }
      return zero + n;
    }
}
