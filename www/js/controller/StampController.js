angular.module('stamp.controllers')
    .controller('StampController', function($ionicPopup, $ionicPlatform, $ionicHistory, $rootScope, $scope, $http, $StorageUtil, $cordovaGeolocation, $ionicGesture, $MathUtill, $cordovaDevice, $ionicModal, $cordovaSocialSharing, $CommonUtil, $ionicSideMenuDelegate, $cordovaToast, $state, $cordovaMedia) {
      //LogUtil.debug("[Stamp Controller]");

        $ionicPlatform.onHardwareBackButton(function() {
            if($ionicHistory.currentStateName() == "app.rally"){
                ionic.Platform.exitApp()
            }else{
                $ionicHistory.goBack();
            }
            e.stopPropagation();
        });

      $scope.stampModalOpen = false;
      $scope.reviewModalOpen = false;
      $scope.eventModalOpen = false;
      $scope.message = "";
      $scope.stampCompleted = false;
      $scope.apply = {};
      $scope.review = {
          'rating': 5
      };

      $ionicModal.fromTemplateUrl('templates/reviewModal.html', {
        scope: $scope
      }).then(function(modal) {
        $scope.reviewModal = modal;
      });

      $ionicModal.fromTemplateUrl('templates/stampModal.html', {
        scope: $scope
      }).then(function(modal) {
        $scope.stampModal = modal;
      });

      $ionicModal.fromTemplateUrl('templates/eventModal.html', {
          scope: $scope
      }).then(function(modal) {
          $scope.eventModal = modal;
      });

      $scope.stampResult = {};

      $scope.touches = {};
      $scope.updateStarted = false;
      $scope.showSpot = ($rootScope.showSpot != undefined) ? $rootScope.showSpot : $StorageUtil.getObject('showSpot');
      $scope.gimbalApiKey = ionic.Platform.isIOS() ? '92c34c46-c29b-46ab-a801-88e29d519ff6' : '6c7f69b7-8573-45e1-afd8-47693c088e59';
      $scope.gimbal = null;

      try {
        Gimbal2.initialize($scope.gimbalApiKey);
        window.addEventListener('beaconsighting', function (sighting) {
          if($scope.gimbal == null || sighting.beaconTemperature > $scope.gimbal.beaconTemperature) {
            $scope.gimbal = sighting;
          }
          //LogUtil.debug('Gimbal - '+sighting.beaconIdentifier+'-'+ sighting.beaconTemperature +'-'+sighting.beaconName);
        });
        Gimbal2.startBeaconManager();
        //LogUtil.debug('Gimbal - '+ Gimbal2.hasInitialized);
      }catch(err) {
        LogUtil.debug('Gimbal - '+ err);
      }

      $scope.$on('$ionicView.loaded', function (viewInfo, state) {
        if($scope.getCheckSpot($scope.showSpot)) $scope.stampCompleted = true;

        var canvas = document.getElementById('stampBack');

        canvas.addEventListener('touchstart', function(event) {
          event.preventDefault();
          $scope.touches = event.touches;
          $scope.touchUpdate();
        });
      });

      $scope.$on('$ionicView.leave', function (viewInfo, state) {
        $scope.closeReviewModal();
        $scope.closeStampModal();
        $scope.closeEventModal();
        Gimbal2.stopBeaconManager();
      });

      $scope.play = function(url) {
        var my_media = new Media(url,
          function () {
            //LogUtil.debug("playAudio() : success");
          },
          function (err) {
            //LogUtil.debug("playAudio() : "+err);
          });
        my_media.play();
      };

      $scope.touchUpdate = function() {
        if ($scope.updateStarted) return;
        $scope.updateStarted = true;

        if($scope.getCheckSpot($scope.showSpot)) {
          $cordovaToast.showLongCenter('이미 스탬프를 찍으셨습니다.');
        }

        try{
          var touchLength = $scope.touches.length;

          //LogUtil.debug('touchLength ' + touchLength);

          if(touchLength >= 3){
            if($rootScope.checkLogin) {
                navigator.vibrate(1000);
              $scope.play(ionic.Platform.isIOS() ? 'sound/stamp.mp3' : '/android_asset/www/sound/stamp.mp3');

              var touchPoint1 = $scope.touches[0]; //원포인트
              var touchPoint2 = $scope.touches[1]; //나머지 점
              var touchPoint3 = $scope.touches[2]; //나머지 점
              var touchPoint4 = touchLength > 3 ? $scope.touches[3] : $scope.touches[2]; //가운데 점

              touchPoint1.x = $scope.touches[0].clientX;
              touchPoint1.y = $scope.touches[0].clientY;
              touchPoint2.x = $scope.touches[1].clientX;
              touchPoint2.y = $scope.touches[1].clientY;
              touchPoint3.x = $scope.touches[2].clientX;
              touchPoint3.y = $scope.touches[2].clientY;
              touchPoint4.x = touchLength > 3 ? $scope.touches[3].clientX : $scope.touches[2].clientX;
              touchPoint4.y = touchLength > 3 ? $scope.touches[3].clientY : $scope.touches[2].clientY;

              var movingPointer = $MathUtill.angleForMove(touchPoint1, touchPoint2, touchPoint3, touchPoint4); //요것이 삼각형을 기울여야하는 각도이셈쿄쿄
              if(movingPointer.success === false){
                movingPointer = $MathUtill.angleForMove(touchPoint1, touchPoint3, touchPoint2, touchPoint4); //요것이 삼각형을 기울여야하는 각도이셈쿄쿄
                if(movingPointer.success === false){
                  movingPointer = $MathUtill.angleForMove(touchPoint1, touchPoint4, touchPoint2, touchPoint3); //요것이 삼각형을 기울여야하는 각도이셈쿄쿄
                  if(movingPointer.success === false) {
                    //LogUtil.debug("[PAGE:STAMP] ALL ERROR");
                  }
                }
              }
              var keyValue1 = {'x':movingPointer.MovPtr.x - movingPointer.fixPtr.x , 'y':movingPointer.MovPtr.y - movingPointer.fixPtr.y};
              var keyValue2 = {'x':movingPointer.ElsPtr.x - movingPointer.fixPtr.x , 'y':movingPointer.ElsPtr.y - movingPointer.fixPtr.y};
              var keyValue3 = {'x':movingPointer.CenPtr.x - movingPointer.fixPtr.x , 'y':movingPointer.CenPtr.y - movingPointer.fixPtr.y};

              var genKeyData = keyValue1.x.toFixed(0) + "_" + keyValue1.y.toFixed(0) + "_" + keyValue2.x.toFixed(0) + "_" + keyValue2.y.toFixed(0) + "_" + keyValue3.x.toFixed(0) + "_" + keyValue3.y.toFixed(0);

              if($cordovaDevice.getPlatform() == "iOS"){
                genKeyData = $scope.getValuePer(keyValue1.x).toFixed(0) + "_"
                  + $scope.getValuePer(keyValue1.y).toFixed(0) + "_"
                  + $scope.getValuePer(keyValue2.x).toFixed(0) + "_"
                  + $scope.getValuePer(keyValue2.y).toFixed(0) + "_"
                  + $scope.getValuePer(keyValue3.x).toFixed(0) + "_"
                  + $scope.getValuePer(keyValue3.y).toFixed(0);
                //LogUtil.debug("[IOS!!!] [ PAGE:STAMP] " + genKeyData);
              }

              var latitude = 0;
              var longitude = 0;
              var geolocationMode = "NO";
              var spotId = 0;

              if($CommonUtil.isRequireGps()){
                if($rootScope.lastLocation != null && $rootScope.lastLocation != undefined) {
                  latitude = $rootScope.lastLocation.latitude;
                  longitude = $rootScope.lastLocation.longitude;
                  geolocationMode = "GPS";
                  spotId = $scope.showSpot.spotId;
                }else{
                  $ionicPopup.alert({
                    title: '알림',
                    template: '현재 위치 정보를 찾을 수 없습니다.<br/>GPS를 반드시 ON 해주시기바랍니다.'
                  });
                  $scope.updateStarted = false;
                  return;
                }
              }
              var req = {
                method: 'POST',
                url: 'https://stamp.2gather.us/api/v1/stamp',
                headers: {
                  'Content-Type': 'application/json; charset=utf-8',
                  'Authorization': 'Bearer ' + $scope.tgToken.access_token
                },
                data: {
                  'clientId': 'GANGJIN',
                  'stampKey': genKeyData,
                  'latitude': latitude,
                  'longitude': longitude,
                  'geolocationMode': geolocationMode,
                  'spotId': spotId,
                  'gimbalKey': ($scope.gimbal != null) ? $scope.gimbal.beaconName : ''
                }
              }
              //LogUtil.debug(JSON.stringify(req));
              $http(req).then(function(response) {
                if(response != undefined) {
                  //LogUtil.debug("STAMP STATUS - " + JSON.stringify(response.data.status));
                  if(response.data.status == 200){
                    $scope.stampResult = response.data;
                    $scope.stampCompleted = true;
                    $scope.stampEvent("SPO");
                  }else{
                    $ionicPopup.alert({
                      title: '알림',
                      template: response.data.message
                    });
                  }
                  $scope.updateStarted = false;
                }
              }, function(err) {
                //LogUtil.error('[STAMPING API HTTP ERROR] - ' + JSON.stringify(err));

                $ionicPopup.alert({
                  title: '알림',
                  template: '스탬프 적립에 실패하였습니다.<br/>다시 확인 바랍니다.'
                });
                $scope.updateStarted = false;
              })
            }else{
              $cordovaToast.showLongCenter("로그인 후 사용가능합니다.");
              $scope.updateStarted = false;
              $scope.goSignIn();
            }
          } else {
            $scope.updateStarted = false;
          }

        }catch(err){
          //LogUtil.error('[Stamp Error]'+err);
          $scope.updateStarted = false;
        }
      }

      $scope.goQRscan = function(rallySpot){

        if($scope.getCheckSpot($scope.showSpot)) {
          $cordovaToast.showLongCenter('이미 스탬프를 찍으셨습니다.');
        }

        if($rootScope.checkLogin) {
          //LogUtil.debug('QR start');
          cordova.plugins.barcodeScanner.scan(
            function (result) {
              if(!result.cancelled)
              {
                if(result.format == "QR_CODE")
                {
                  //LogUtil.debug('QR data-'+ result.text);
                  navigator.vibrate(1000);
                  $scope.qrKey = result.text;

                  var latitude = 0;
                  var longitude = 0;
                  var geolocationMode = "NO";
                  var spotId = 0;

                  if($CommonUtil.isRequireGps()){
                    if($rootScope.lastLocation != null && $rootScope.lastLocation != undefined) {
                      latitude = $rootScope.lastLocation.latitude;
                      longitude = $rootScope.lastLocation.longitude;
                      geolocationMode = "GPS";
                      spotId = $scope.showSpot.spotId;
                    }else{
                      $ionicPopup.alert({
                        title: '알림',
                        template: '현재 위치 정보를 찾을 수 없습니다.<br/>GPS를 반드시 ON 해주시기바랍니다.'
                      });
                      //LogUtil.debug("위치 정보를 찾을수 없다");
                      return;
                    }
                  }
                  var req = {
                    method: 'POST',
                    url: 'https://stamp.2gather.us/api/v1/stamp',
                    headers: {
                      'Content-Type': 'application/json; charset=utf-8',
                      'Authorization': 'Bearer ' + $scope.tgToken.access_token
                    },
                    data: {
                      'clientId': 'GANGJIN',
                      'stampKey': '0_0_0_0_0_0',
                      'latitude': latitude,
                      'longitude': longitude,
                      'geolocationMode': geolocationMode,
                      'spotId': spotId,
                      'qrKey': $scope.qrKey
                    }
                  }
                  //LogUtil.debug(''+JSON.stringify(req));
                  $http(req).then(function(response) {
                    if(response != undefined){
                      LogUtil.debug("STAMP STATUS - " + JSON.stringify(response.data.status));
                      if(response.data.status == 200){
                        $scope.stampResult = response.data;
                        $scope.stampCompleted = true;
                        $scope.stampEvent("SPO");
                      }else{
                        $ionicPopup.alert({
                          title: '알림',
                          template: (response.data.message == '' || response.data.message == undefined) ? '스탬프 적립에 실패하였습니다.' : response.data.message
                        });
                      }
                    }
                  }, function(err) {
                    //LogUtil.error('[STAMPING API HTTP ERROR] - ' + JSON.stringify(err));
                    $ionicPopup.alert({
                      title: '알림',
                      template: '스탬프 적립에 실패하였습니다.<br/>다시 확인 바랍니다.'
                    });
                  })
                }
              }
            },
            function (error) {
              //LogUtil.error("QR Error");
            }
          );
        } else {
          $scope.goSignIn();
        }
      }

      $scope.goSpotDetail = function(rallySpot){
          //LogUtil.debug('Go Spot Detail - '+JSON.stringify(rallySpot));
          $rootScope.showSpot = rallySpot;
          $rootScope.titleName = rallySpot.spotName;
          $state.transitionTo('app.rallyDetail', { rallyId: rallySpot.spotId });
      }

      $scope.getValuePer = function (value) {
        var per = 0.2;
        return value - (value * per)
      }

      $scope.shareFacebook = function() {
        //LogUtil.debug("[[shareFacebook]]");
        try {
          $cordovaSocialSharing
            .shareViaFacebook($scope.showSpot.spotDescriptionShort + " - " + $scope.showSpot.spotName, "https://stamp.2gather.us/image/"+$scope.showSpot.image[1].imageName+"."+$scope.showSpot.image[1].extension, "")
            .then(function(result) {
              //LogUtil.debug(JSON.stringify(result));
              // Success!
            }, function(err) {
              //LogUtil.debug("[[shareFacebook]]Err"+JSON.stringify(err));
              // An error occurred. Show a message to the user
            });
        } catch(err) {
          //LogUtil.debug("[[shareFacebook]]Exp"+JSON.stringify(err));
        }
      };

      $scope.shareTwitter = function() {
        //LogUtil.debug("[[shareTwitter]]");
        try {
          $cordovaSocialSharing
            .shareViaTwitter($scope.showSpot.spotDescriptionShort + " - " + $scope.showSpot.spotName, "https://stamp.2gather.us/image/"+$scope.showSpot.image[1].imageName+"."+$scope.showSpot.image[1].extension, "")
            .then(function(result) {
              //LogUtil.debug(JSON.stringify(result));
              // Success!
            }, function(err) {
              //LogUtil.debug("[[shareTwitter]]Err"+JSON.stringify(err));
              // An error occurred. Show a message to the user
            });
        }catch(err) {
          //LogUtil.debug("[[shareTwitter]]Exp"+JSON.stringify(err));
        }
      };

      $scope.shareNative = function() {
        //LogUtil.debug("[[shareNative]]");
        try {
          $cordovaSocialSharing
            .share($scope.showSpot.spotDescriptionShort + " - " + $scope.showSpot.spotName, "https://stamp.2gather.us/image/"+$scope.showSpot.image[0].imageName+"."+$scope.showSpot.image[0].extension, "https://stamp.2gather.us/image/"+$scope.showSpot.image[1].imageName+"."+$scope.showSpot.image[1].extension, "") // Share via native share sheet
            .then(function(result) {
              // Success!
            }, function(err) {
              //LogUtil.debug("[[shareNative]]Err"+JSON.stringify(err));
            });
        }catch(err) {
          //LogUtil.debug("[[shareNative]]Exp"+JSON.stringify(err));
        }
      };

      $scope.stampEvent = function(eventType) {
        $http({
          method: 'POST',
          url: 'https://stamp.2gather.us/api/v1/rallyEvents/join',
          headers: {
            'Content-Type': 'application/json; charset=utf-8',
            'Authorization': 'Bearer ' + $scope.tgToken.access_token
          },
          data: {
            'rallyId': $scope.showSpot.rallyId,
            'eventType': eventType
          }
        }).then(function(response) {
          //LogUtil.debug('stamp event response ' +  JSON.stringify(response));
          if(response != undefined){
            //LogUtil.debug('stamp event OK ' +  JSON.stringify(response.data.list));
            var eventList = response.data.list;
            if(eventList.length > 0) {
              var msg = '';
              var today = new Date();

              for (var i = 0; i < eventList.length; i++) {
                  //LogUtil.debug(JSON.stringify(eventList[i]));
                  //LogUtil.debug($scope.toDate(eventList[i].startDate) +'|'+ today +'|'+ $scope.toDate(eventList[i].endDate));
                  if($scope.toDate(eventList[i].startDate) <= today && today <= $scope.toDate(eventList[i].endDate)) {
                      if(eventType == 'SPO') $scope.openEventModal();
                      //msg = '이벤트에 성공적으로 응모되었습니다.';
                      //if (i == 0) {
                      //    msg += eventList[i].eventMessage;
                      //} else {
                      //    msg += ',' + eventList[i].eventMessage;
                      //}
                  }
              }

              //if(msg != '') {
              //     if(eventType == 'SPO') $scope.openEventModal();
              //     $ionicPopup.alert({
              //         title: '이벤트 응모',
              //         template: msg
              //     });
              // }
            }
          }
        }, function(err) {
          //LogUtil.debug('stampEvent ERROR - ' + err);
        })
      }

      $scope.spotEvent = function() {
          $http({
              method: 'POST',
              url: 'https://stamp.2gather.us/api/v1/rallyEvents/spotCheck',
              headers: {
                  'Content-Type': 'application/json; charset=utf-8',
                  'Authorization': 'Bearer ' + $scope.tgToken.access_token
              },
              data: {
                  'rallyId': $scope.showSpot.rallyId
              }
          }).then(function(response) {
              if(response != undefined){
                  if(response.status == 200) {
                      if(response.data > 11) $scope.openEventModal();
                  }
              }
          }, function(err) {
          });
      }


    $scope.checkReview = function() {
        try {
            $http({
                method: 'POST',
                url: 'https://stamp.2gather.us/api/v1/spotReviews/check',
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Authorization': 'Bearer ' + $scope.tgToken.access_token
                },
                data: {
                    'rallyId': $scope.showSpot.rallyId,
                    'spotId': $scope.showSpot.spotId
                }
            }).then(function(response) {
                //LogUtil.debug(JSON.stringify(response));
                if(response != undefined){
                    if(response.data != ''){
                        $scope.review = response.data;
                    }
                }
            }, function(err) {
                LogUtil.debug('checkReview ERR' + JSON.stringify(err));
            })
        } catch(e) {
            LogUtil.debug('checkReview Exception' + e);
        }

    };

      $scope.writeReview = function() {
        try {
          $http({
            method: 'POST',
            url: 'https://stamp.2gather.us/api/v1/spotReviews',
            headers: {
              'Content-Type': 'application/json; charset=utf-8',
              'Authorization': 'Bearer ' + $scope.tgToken.access_token
            },
            data: {
              'rallyId': $scope.showSpot.rallyId,
              'spotId': $scope.showSpot.spotId,
              'spotReviewId': $scope.review.spotReviewId,
              'rating': $scope.review.rating,
              'content': $scope.review.content,
              'reviewType': 'BASIC'
            }
          }).then(function(response) {
            //LogUtil.debug("writeReview response " +  JSON.stringify(response));
            if(response != undefined){
              if(response.data.status == 200){
                //LogUtil.debug("writeReview OK " +  JSON.stringify(response.data));
                $scope.stampEvent("REV");
                $scope.closeReviewModal();
                $cordovaToast.showLongCenter('리뷰가 등록되었습니다.');
                $scope.checkReview();
              }
            }
          }, function(err) {
              $cordovaToast.showLongCenter('리뷰 등록에 실패했습니다.');
            //LogUtil.debug('writeReview ERR' + err);
          })
        } catch(e) {
            $cordovaToast.showLongCenter('리뷰 등록에 실패했습니다.');
          //LogUtil.debug('writeReview Exception' + e);
        }

      };

      // $scope.openStampModal = function() {
      //   document.getElementsByClassName('bar bar-light-stamp')[1].style.visibility = 'hidden';
      //   $scope.stampModalOpen = true;
      //   $scope.stampModal.show();
      //   $scope.joinEvent('SPO');
      // }
      //
      // $scope.closeStampModal = function() {
      //   document.getElementsByClassName('bar bar-light-stamp')[1].style.visibility = 'visible';
      //   $scope.stampModalOpen = false;
      //   $scope.stampModal.hide();
      // }

      $scope.openReviewModal = function(){
        if($rootScope.checkLogin) {
          $scope.reviewModalOpen = true;
          $scope.reviewModal.show();
          $scope.setStar($scope.review.rating);
          //document.getElementById("reviewText").value = '';
        }else{
          $scope.goSignIn();
        }
      }

      $scope.closeReviewModal = function() {
        $scope.reviewModalOpen = false;
        $scope.reviewModal.hide();
      };

      $scope.openStampModal = function(){
          if($rootScope.checkLogin) {
              $scope.stampModalOpen = true;
              $scope.stampModal.show();
          }else{
              $scope.goSignIn();
          }
      }

      $scope.closeStampModal = function() {
          $scope.stampModalOpen = false;
          $scope.stampModal.hide();
      };

    $scope.openEventModal = function(){
        $scope.eventModalOpen = true;
        $scope.eventModal.show();
    }

    $scope.closeEventModal = function() {
        $scope.eventModalOpen = false;
        $scope.eventModal.hide();
    };

    $scope.setStar = function(value){
        if($rootScope.checkLogin) {
          var img = document.getElementsByClassName('starImg');
          for(var i = 0 ; i < img.length; i++){
            if(i < value) {
              img[i].src = "img/star_full.png"
            } else {
              img[i].src = "img/star_zero.png"
            }
          }
            LogUtil.debug('star'+value);
          $scope.review.rating = value;
        }else{
          $scope.goSignIn();
        }
    }

      $scope.getCheckSpot = function(rallySpot){
        if(!$rootScope.checkLogin || rallySpot.stampId == undefined){
          return false;
        }
        return true;
      }

      $scope.doApply = function(){
          if($scope.apply.name == ''){
              $cordovaToast.showLongCenter("이름을 입력하셔야 응모하실 수 있습니다.");
              return
          }

          if($scope.apply.phone == ''){
              $cordovaToast.showLongCenter("연락처를 입력하셔야 응모하실 수 있습니다.");
              return
          }

          if($scope.apply.address == ''){
              $cordovaToast.showLongCenter("주소를 입력하셔야 응모하실 수 있습니다.");
              return
          }
            try {
                $http({
                    method: 'POST',
                    url: 'https://stamp.2gather.us/api/v1/rallyEvents/apply',
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        'Authorization': 'Bearer ' + $scope.tgToken.access_token
                    },
                    data: {
                        'rallyId': $scope.showSpot.rallyId,
                        'eventId': 4,
                        'name': $scope.apply.name,
                        'phone': $scope.apply.phone,
                        'address': $scope.apply.address
                    }
                }).then(function(response) {
                    if(response != undefined){
                        if(response.status == 200){
                            $cordovaToast.showLongCenter("이벤트에 성공적으로 응모되셨습니다.");
                            $scope.closeEventModal();
                        }else{
                            $cordovaToast.showLongCenter('이벤트 응모에 실패했습니다.');
                        }
                    }
                }, function(err) {
                    $cordovaToast.showLongCenter('이벤트 응모에 실패했습니다.');
                });
            } catch(e) {
                $cordovaToast.showLongCenter('이벤트 응모에 실패했습니다.');
            }
      }

      $scope.checkReview();

        $scope.toDate = function(str) {
            return Date.parse(str.substring(0, 4)+'-'+str.substring(4,6)+'-'+str.substring(6,8));
        }
    });
