angular.module('stamp.controllers')
.controller('RallyController', function($rootScope, $scope, $http, $StorageUtil, $state, $ionicSideMenuDelegate, $cordovaToast, $CommonUtil, $ionicModal) {

  //LogUtil.debug("[Rally Controller]");

  $scope.rallySpotList = [];
  //$scope.rallyData = [];
  $scope.onLoadEventCheck = false;
  $scope.message = "";
  $scope.error = "";

    $scope.noticePopup = {};
    $scope.noticeModalOpen = false;

    $ionicModal.fromTemplateUrl('templates/noticeModal.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.noticeModal = modal;
    });

    $scope.openNoticeModal = function(){
        $scope.noticeModalOpen = true;
        $scope.noticeModal.show();
    }

    $scope.closeNoticeModal = function() {
        $StorageUtil.set($scope.noticePopup.notice_id, $scope.noticePopup.nomore);
        $scope.noticeModalOpen = false;
        $scope.noticeModal.hide();
    };

  $scope.$on('onLoginUser', function(e) {
    //LogUtil.debug("[Rally onLoginUser]");
    $scope.getRallySpot();
  });

  $scope.$on('$ionicView.enter', function (viewInfo, state) {
    if($scope.onLoadEventCheck){
      $scope.getRallySpot();
    }
  });

  $scope.$on('$ionicView.loaded', function (viewInfo, state) {
    $scope.rallySpotList = [{"rallyId":500,"spotId":1,"stampId":undefined},{"rallyId":500,"spotId":2,"stampId":undefined},{"rallyId":500,"spotId":3,"stampId":undefined},{"rallyId":500,"spotId":4,"stampId":undefined},{"rallyId":500,"spotId":5,"stampId":undefined},{"rallyId":500,"spotId":6,"stampId":undefined},{"rallyId":500,"spotId":7,"stampId":undefined},{"rallyId":500,"spotId":8,"stampId":undefined},{"rallyId":500,"spotId":9,"stampId":undefined},{"rallyId":500,"spotId":10,"stampId":undefined},{"rallyId":500,"spotId":11,"stampId":undefined},{"rallyId":500,"spotId":12,"stampId":undefined}];

    //LogUtil.debug("[Rally onLoad]");

    $scope.onLoadEventCheck = true;
    $scope.getRallySpot();


  });

  $scope.getRallySpot = function(){
    $scope.data = { };

    if($rootScope.checkLogin) {
      $scope.data = {
        method: 'GET',
        url: 'https://stamp.2gather.us/api/v1/rallys/500/spots',
        headers: {
          'Content-Type': 'application/json; charset=utf-8',
          'Authorization': 'Bearer ' + $scope.tgToken.access_token
        }
      }
    } else {
      $scope.data = {
        method: 'GET',
        url: 'https://stamp.2gather.us/openapi/spots/500'
      }
    }

    try{
        //LogUtil.debug("[getRallySpot]"+JSON.stringify($scope.data));
      $http($scope.data).then(function(response) {
        if(response != undefined){
          $rootScope.rallySpotList = $scope.rallySpotList = response.data.list;
        }
      }, function(err) {
        $rootScope.rallySpotList = $scope.rallySpotList;
        //$cordovaToast.showLongCenter('관리자에게 문의해주세요.\n잘못된 요청입니다.');
        //LogUtil.debug("[getRallySpot Error]"+err);
      })

    }catch(err){
      $rootScope.rallySpotList = $scope.rallySpotList;
      //$cordovaToast.showLongCenter('관리자에게 문의해주세요.\n서버에 요청중 문제가 생겼습니다.');
      //LogUtil.error("[getRallySpot Exception]" + JSON.stringify(err));
    }
  }

  $scope.getColCss = function(rallySpot){
    if(rallySpot.spotId % 3 == 1){
      if(Math.ceil(rallySpot.spotId / 3) % 2 == 0 ){
        //짝
        return "firstEvenRow"
      }else{
        //홀
        return "firstOddRow"
      }
    }
    if(Math.ceil(rallySpot.spotId / 3) % 2 == 0 ){
      //짝
      return "evenRow"
    }else{
      //홀
      return "oddRow"
    }
  }

  $scope.getCheckSpot = function(rallySpot){
    //LogUtil.debug("[Rally]CheckSpot-"+$rootScope.checkLogin +","+rallySpot.stampId);
    if($rootScope.checkLogin && rallySpot.stampId != undefined) {
      return 'c';
    } else {
      return '';
    }
  }

  $scope.goStamp = function(rallySpot){
    //if($rootScope.checkLogin) {
      $rootScope.showSpot = rallySpot;
      $rootScope.titleName = rallySpot.spotName;
      $StorageUtil.setObject('showSpot', $rootScope.showSpot);
      $state.transitionTo("app.stamp", {rallyId: rallySpot.spotId});
  }

    try{
        $http({
            method: 'GET',
            url: 'https://stamp.2gather.us/openapi/notice/500'
        }).then(function(response) {
            if(response != undefined){
                $scope.noticeList = response.data.list;
                for(var i in $scope.noticeList) {
                    var notice = $scope.noticeList[i];
                    if(notice.popup == '1') {
                        $scope.noticePopup = notice;
                        if($StorageUtil.get($scope.noticePopup.notice_id) != 'true'){
                            $scope.openNoticeModal();
                            break;
                        }
                    }
                }
                //LogUtil.debug("[Notice List]"+ JSON.stringify(response.data.list));
            }
        }, function(err) {
            //LogUtil.debug('[Notice List Error]', err);
        })

    }catch(err){
        //LogUtil.debug("[Notice List Exception]" + JSON.stringify(err));
    }
});
