angular.module('stamp.controllers')
.controller('NoticeController', function($ionicPlatform, $ionicHistory, $rootScope, $scope, $http, $StorageUtil, $ionicModal, $CommonUtil) {

  //LogUtil.debug("[Notice Controller]");

    $ionicPlatform.onHardwareBackButton(function(e) {
        if($ionicHistory.currentStateName() == "app.rally"){
            ionic.Platform.exitApp()
        }else{
            $ionicHistory.goBack();
        }
        e.stopPropagation();
    });

  $scope.$on('$ionicView.loaded', function (viewInfo, state) {
  });

  try{
    $http({
      method: 'GET',
      url: 'https://stamp.2gather.us/openapi/notice/500'
    }).then(function(response) {
      if(response != undefined){
        $scope.noticeList = response.data.list;
        //LogUtil.debug("[Notice List]"+ JSON.stringify(response.data.list));
      }
    }, function(err) {
      //LogUtil.debug('[Notice List Error]', err);
    })

  }catch(err){
    //LogUtil.debug("[Notice List Exception]" + JSON.stringify(err));
  }
});
